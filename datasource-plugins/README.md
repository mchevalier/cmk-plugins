# Custom Datasource Plugins

Check**MK** will treat any output that is formated in agent format as an agent 
check. This allows us to extend the agent based monitoring to check just about
anything.

## References

  - https://checkmk.com/cms_datasource_programs.html