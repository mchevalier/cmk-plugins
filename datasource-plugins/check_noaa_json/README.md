![Python](https://img.shields.io/badge/python-v2-blue.svg)
![Dependencies](https://img.shields.io/badge/dependencies-up%20to%20date-green.svg)
![Contributions welcome](https://img.shields.io/badge/contributions-welcome-orange.svg)


# Check_NOAA_JSON

Check_MK datasource plugin for checking NOAA JSON statistics

## Requirements
  * Python 2
  * Check_MK

## Installation

  1. install plugins from ```cmk/``` directory to 
```~/local/share/check_mk/checks```
  2. install **noaa_electron_fluence_forecast.py** in ```/usr/bin``` and make it exectutable
> chmod +x /usr/bin/noaa_electron_fluence_forecast.py

  3. in CMK WATO make rule to use datasource program instead of cmk agent, enter
```noaa_electron_fluence_forecast``` as argument
  * Please note you will need to make a rule for each datasource check
