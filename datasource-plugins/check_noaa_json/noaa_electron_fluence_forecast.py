#!/bin/env python2
# Check_NOAA_Electron_Fluence_Forecast
# Author: Matt Chevalier
#   Date: 05/16/2019
#  DDate: 63-2-3185

import urllib, json

# CMK Agent Header
print '<<<noaa_electron_fluence_forecast>>>'

# Get the data from the interwebz
url = "https://services.swpc.noaa.gov/json/electron_fluence_forecast.json"
response = urllib.urlopen(url)
data = json.loads(response.read())

n = 0 # Instantiate index counter

while True:
        try:        
                # Extract nested data to variables
                date = str(data[n]['date'])
                speed = str(data[n]['speed'])
                fluence = str(data[n]['fluence'])
                fluence_day_two = str(data[n]['fluence_day_two'])
                fluence_day_three = str(data[n]['fluence_day_three'])
                fluence_day_four = str(data[n]['fluence_day_four'])

                print '%s %s %s %s %s %s' % (date, speed, fluence, fluence_day_two, fluence_day_three, fluence_day_four)
                n += 1
        except:
                exit(1)