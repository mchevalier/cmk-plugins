![Python](https://img.shields.io/badge/python-v2-blue.svg)

# OctopiScrape

CMK plugin for checking data from Octopi

## Requirements
  * Python 2
  * Check_MK

## Installation

  1. install plugins from ```cmk/``` directory to ```~/local/share/check_mk/checks```
  2. install **octopiScrape** in ```/usr/bin``` and make it exectutable
> chmod +x /usr/bin/octopiScrape

  3. in CMK WATO make rule to use datasource program instead of cmk agent, enter ```octopiScrape -H $HOSTNAME$ -k <api_key>``` as argument
  * Please note you will need to make a rule for each host if the API keys differ between hosts
