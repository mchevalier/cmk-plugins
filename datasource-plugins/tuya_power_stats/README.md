![Python](https://img.shields.io/badge/python-v3-green.svg)
![Contributions welcome](https://img.shields.io/badge/contributions-welcome-orange.svg)

# Tuya_Power_Stats

Check_MK datasource plugin for checking tuya smart socket statistics

## Requirements
  * Python 3
  * Check_MK
  * Device ID
  * Device IP

## Installation

  1. install plugins from ```cmk/``` directory to 
```~/local/share/check_mk/checks```
  2. install **tuya_power_stats** in ```/usr/bin``` and make it exectutable
> chmod +x /usr/bin/tuya_power_stats

  3. in CMK WATO make rule to use datasource program instead of cmk agent, enter
```tuya_power_stats -H $HOSTADDRESS$ -i <device_id>``` as argument
  * Please note you will need to make a rule for each host as the Device ID's 
are unique
