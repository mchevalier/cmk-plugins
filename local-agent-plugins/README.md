# Local Checks

## Output Format

``0      myservice   myvalue=73;80;90  My output text which may contain spaces``

The four parts are separated by blanks and have the following meanings: 

1. 	**Status** 	The status of a service is given as a number: 0 for OK, 1 for WARN, 2 for CRIT and 3 for UNKNOWN.
2. 	**Service name** 	The service name as shown in Check**MK**. This may not contain blanks.
3. 	**Metrics** 	Performance values for the data. More information about the construction can be found later below. Alternatively a minus sign can be coded if the check produces no metrics.
4. 	**Status details** 	Details for the status as they will be shown in Check_MK. This part can also contain blanks.

## Installation

1. Place check file into ``/usr/lib/check_mk_agent/local/``
2. Make executable ``chmod +x /usr/lib/check_mk_agent/local/<check-file>``
3. Rediscover services in Check**MK**, and enable monitoring your new checks

## References
  
  - https://checkmk.com/cms_localchecks.html