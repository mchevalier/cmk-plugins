![Golang](https://img.shields.io/badge/golang-v1.12.7-blue.svg)

# Check_DHT11
Checks Humidity and Temperature from a DHT11 Sensor

## Prerequisites
  - Check_MK
  - DHT11 Sensor ``On GPIO pin 4``
  - Raspberry Pi
  - Golang ``Optional - If you want to build binary from source``

## Installation
  1. Install check_dht11 into ``/usr/lib/check_mk_agent/local/``
  2. Make script executable ``sudo chmod +x /usr/lib/check_mk_agent/local/check_dht11``
  3. Run Check_MK Agent Discovery again

## References
  - http://www.circuitbasics.com/how-to-set-up-the-dht11-humidity-sensor-on-the-raspberry-pi/