package main

import (
        "github.com/d2r2/go-dht"
        logger "github.com/d2r2/go-logger"
        "fmt"
)

var lg = logger.NewPackageLogger("main",
        logger.DebugLevel,
        // logger.InfoLevel,
)

func main() {
        defer logger.FinalizeLogger()

        // sensorType := dht.DHT11
        // sensorType := dht.AM2302
        sensorType := dht.DHT22
        // Read DHT11 sensor data from specific pin, retrying 10 times in case of failure.
        // Uncomment/comment next line to suppress/increase verbosity of output
        logger.ChangePackageLogLevel("dht", logger.InfoLevel)
        // Set GPIO pin and retry count
        pin := 4
        retry := 10

        temperature, humidity, retried, err :=
                dht.ReadDHTxxWithRetry(sensorType, pin, false, retry)
        if err != nil {
                // fmt.Println(err)
        }
        if retried < 11 {
                // fmt.Println(err)
        }
        // Check for non-negative value
        if humidity < 0 {
                main() // Run check again until appropriate value is found
        } else {
                // print temperature and humidity
                fmt.Printf("0 PiTemp Temp=%v %vC\n", temperature, temperature)
                fmt.Printf("0 PiHumidity Humidity=%v %v%%\n", humidity, humidity)
                // fmt.Printf("RETRIES: %v\n", retried)
        }
}
