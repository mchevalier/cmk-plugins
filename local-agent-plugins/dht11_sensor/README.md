![Python](https://img.shields.io/badge/python-v3.7-green.svg)

# DHT11 Sensor
Checks Humidity and Temperature from a DHT11 Sensor

## NOTE: A new version of this check has been written in golang
[check_dht11](../check_dht11/)

## Prerequisites
  - Check_MK
  - DHT11 Sensor
  - Raspberry Pi
  - Python 3

## Installation
  1. Install python3 and pip ``sudo apt install python3 python3-pip``
  2. Install requirements ``sudo pip install -r requirements.txt``
  3. Install dht11_sensor.py into ``/usr/lib/check_mk_agent/local/``
  4. Make script executable ``sudo chmod +x /usr/lib/check_mk_agent/local/dht11_sensor.py``
  5. Run Check_MK Agent Discovery again

## References
  - http://www.circuitbasics.com/how-to-set-up-the-dht11-humidity-sensor-on-the-raspberry-pi/