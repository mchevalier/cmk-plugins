#!/usr/bin/python3
#Author: Matt Chevalier
#  Date: 06/10/2019
import sys, Adafruit_DHT
from statistics import mode

#Set an odd number for retry else, the mode may fail for evenly split values 
retry = 5
read = 0

humidity_list = []
temperature_list = []

# Get a list of values to find the mode
# This helps prevent the faulty readings
while read < retry:
    humidity, temperature = Adafruit_DHT.read_retry(11, 4)
    read += 1
    #print(read, humidity, temperature)
    humidity_list.append(humidity)
    temperature_list.append(temperature)

humidity = mode(humidity_list)
temperature = mode(temperature_list)

print('0 PiTemp Temp={0:0.1f} {0:0.1f}C'.format(temperature, temperature))
print('0 PiHumidity Humidity={1:0.1f} {1:0.1f}%'.format(humidity, humidity))